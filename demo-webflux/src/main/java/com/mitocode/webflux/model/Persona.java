package com.mitocode.webflux.model;

public class Persona {

	public Persona(Integer idPersona, String nombre) {
		this.idPersona = idPersona;
		this.nombre = nombre;
	}

	private Integer idPersona;
	private String nombre;

	public Integer getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Integer idPersona) {
		this.idPersona = idPersona;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nobmre) {
		this.nombre = nobmre;
	}

}
