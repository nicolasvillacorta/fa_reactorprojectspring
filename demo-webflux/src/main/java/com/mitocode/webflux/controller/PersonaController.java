package com.mitocode.webflux.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.webflux.model.Persona;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/personas")
public class PersonaController {

	/*
	 * Por default ya todo este codigo funciona de manera asincrona, por lo tanto tiene mejor performance que SpringMVC.
	 * Las soluciones aca se piensan no de manera imperativa, sino funcional.
	 * 
	 * Spring Webflux tambien tiene una forma distinta de manejar el routing, que es sin anotaciones, sino desde una clase de Configuracion.	
	 * */
	private static final Logger LOGGER = LoggerFactory.getLogger(PersonaController.class);
	
	@GetMapping("/response")
	public Mono<ResponseEntity<Flux<Persona>>> listarEntity(){
		List<Persona> personas = new ArrayList<Persona>();
		personas.add(new Persona(1, "Mito"));
		personas.add(new Persona(2, "Code"));
		
		Flux<Persona> personasFlux = Flux.fromIterable(personas);
		
		return Mono.just(ResponseEntity.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(personasFlux));
	}
	
	
	@GetMapping("/mostrar")
	public Mono<Persona> mostrar(){
		return Mono.just(new Persona(1, "Nicolas"));
	}
	
	@GetMapping
	public Flux<Persona> listar(){
		List<Persona> personas = new ArrayList<Persona>();
		personas.add(new Persona(1, "Mito"));
		personas.add(new Persona(2, "Code"));
		
		Flux<Persona> personasFlux = Flux.fromIterable(personas);
		return personasFlux;
	}
	
	@DeleteMapping("/{modo}")
	public Mono<ResponseEntity<Void>> eliminar(@PathVariable("modo") Integer modo){
		return buscarPersona(modo)
				.flatMap(p -> {
					return eliminar(p)
							.then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)));
				}).defaultIfEmpty(new ResponseEntity<Void>(HttpStatus.NOT_FOUND));
	}
	
	// Estos 2 proximos metodos simularian que conecta a una base de datos.
	public Mono<Void> eliminar(Persona p){
		LOGGER.info("Eliminando " + p.getIdPersona() + " - " + p.getNombre());
		
		return Mono.empty();
	}
	
	public Mono<Persona> buscarPersona(Integer modo){
		
		if(modo == 1) {
			return Mono.just(new Persona(1, "MitoCode"));
		} else {
			return Mono.empty();
		}	
		
	}
	
}
